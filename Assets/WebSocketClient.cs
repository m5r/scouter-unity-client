﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WebSocketSharp;
using System.Text;

public class WebSocketClient : MonoBehaviour {
	// Use this for initialization
	public GameObject emptyGameObjectPrefab;
	public WebSocket ws;
	private Queue<Notification> notifications;

	void Start () {
		notifications = new Queue<Notification>();
		ws = new WebSocket ("ws://165.227.237.182:3029");

		ws.OnOpen += (sender, e) => {
			Debug.Log ("Unity client connected to ws server");
		};

		ws.OnError += (sender, e) => {
			Debug.LogError ("Error occurred: " + e.Message);
		};

		ws.OnClose += (sender, e) => {
			Debug.LogError ("Closed: " + e.Code);
		};

		ws.OnMessage += (sender, e) => {
			if (e.IsText) {
				Debug.Log ("message: " + e.Data);
			}

			if (e.IsBinary) {
				string jsonStr = Encoding.UTF8.GetString(e.RawData);
				Notification notification = JsonUtility.FromJson<Notification>(jsonStr);
				Debug.Log ("origin: " + notification.origin);
				Debug.Log ("title: " + notification.title);
				Debug.Log ("content: " + notification.content);

				notifications.Enqueue(notification);
			}
		};

		ws.Connect ();
	}
	
	// Update is called once per frame
	void Update () {
		if (notifications.Count > 0) {
			Notification notification = notifications.Dequeue();
			TextMesh[] texts = emptyGameObjectPrefab.GetComponentsInChildren<TextMesh>();
			texts[0].text = notification.origin;
			texts[1].text = notification.title;
			texts[2].text = notification.content;
			Instantiate(emptyGameObjectPrefab, transform.position,Quaternion.identity);
		}
	}

	void OnDestroy () {
		if (ws != null) {
			ws.Close();
		}
	}
}
